extends Node2D

enum powers {NONE, LOOT}

var mainRef
var imageSprite := 'A'
var animals := {'A': 0, 'B': 0, 'C': 0, 'D': 0}
var trees := {'A': 0, 'B': 0, 'C': 0, 'D': 0}
var power := powers.NONE
var locked := false
var handPosition : Vector2

func init(main, cardDef, pos:Vector2):
	mainRef = main
	self.imageSprite = cardDef.imageSprite
	self.animals = cardDef.animals
	self.trees = cardDef.trees
	self.power = cardDef.power
	set_basic_pos(pos)
	load_images()

func load_images():
	var background_texture = load('res://sprites/forest' + imageSprite + '.jpg')
	$ForestSprite.set_texture(background_texture)
	var badge_index := 0
	for tree in trees :
		for index in range(trees[tree]) :
			var temp_texture = load('res://sprites/treeBadge' + tree + '.svg')
			$Badges.get_child(badge_index).set_texture(temp_texture)
			$Badges.get_child(badge_index).visible = true
			badge_index += 1
	for animal in animals :
		for index in range(animals[animal]) :
			var temp_texture = load('res://sprites/animalBadge' + animal + '.svg')
			$Badges.get_child(badge_index).set_texture(temp_texture)
			$Badges.get_child(badge_index).visible = true
			badge_index += 1

func set_basic_pos(pos:Vector2):
	handPosition = pos
	self.position = handPosition

func played():
	locked = true
	$Area2D.input_pickable = false

func _on_area_2d_input_event(_viewport, event, _shape_idx):
	if event is InputEventMouseButton  and event.pressed :
		if event.button_index == MOUSE_BUTTON_LEFT and !locked and !Lock.lock :
			mainRef.pickup_card(self)
		if event.button_index == MOUSE_BUTTON_RIGHT :
			self.position = self.handPosition
			mainRef.drop_card(self)
