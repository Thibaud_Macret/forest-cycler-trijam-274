extends Area2D

@export var zone:int

var popup_time := 0.0

func _ready():
	var background_texture = load('res://sprites/zone' + str(zone) + '.jpg')
	$Zone.set_texture(background_texture)

func _on_mouse_entered():
	$ColorRect.visible = true

func _on_mouse_exited():
	$ColorRect.visible = false

func _process(delta):
	if(popup_time > 0) :
		popup_time -= delta
		if(popup_time < 0) :
			popup_time = 0
			$Score.visible = false
			$Seeds.visible = false
