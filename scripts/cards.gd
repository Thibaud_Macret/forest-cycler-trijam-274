extends Node

var basicCards := [
	{'imageSprite': 'A', 'trees': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'animals': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'A', 'trees': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'A', 'trees': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'power': 0},
	{'imageSprite': 'A', 'trees': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'power': 0},
	{'imageSprite': 'B', 'trees': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'animals': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'B', 'trees': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'B', 'trees': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'power': 0},
	{'imageSprite': 'B', 'trees': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'power': 0},
	{'imageSprite': 'C', 'trees': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'animals': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'C', 'trees': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'animals': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'C', 'trees': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'power': 0},
	{'imageSprite': 'C', 'trees': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'power': 0},
	{'imageSprite': 'D', 'trees': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'animals': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'D', 'trees': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'animals': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'D', 'trees': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'animals': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'power': 0},
	{'imageSprite': 'D', 'trees': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'animals': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'power': 0},
	
	{'imageSprite': 'G', 'trees': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'power': 0},
	{'imageSprite': 'G', 'trees': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'power': 0},
	{'imageSprite': 'G', 'trees': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'animals': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'G', 'trees': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'animals': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'F', 'trees': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'animals': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'power': 1},
	{'imageSprite': 'F', 'trees': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'power': 1},
	{'imageSprite': 'F', 'trees': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'power': 1},
	{'imageSprite': 'F', 'trees': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'animals': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'power': 1},
	{'imageSprite': 'F', 'trees': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'power': 1},
	{'imageSprite': 'F', 'trees': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'power': 1},
	{'imageSprite': 'F', 'trees': {'A': 0, 'B': 0, 'C': 1, 'D': 0}, 'animals': {'A': 1, 'B': 0, 'C': 0, 'D': 0}, 'power': 1},
	{'imageSprite': 'F', 'trees': {'A': 0, 'B': 0, 'C': 0, 'D': 1}, 'animals': {'A': 0, 'B': 1, 'C': 0, 'D': 0}, 'power': 1},
]

var fourTreeCards := [
	{'imageSprite': 'E', 'trees': {'A': 2, 'B': 0, 'C': 0, 'D': 0}, 'animals': {'A': 1, 'B': 1, 'C': 0, 'D': 0}, 'power': 0},
	{'imageSprite': 'E', 'trees': {'A': 0, 'B': 2, 'C': 0, 'D': 0}, 'animals': {'A': 0, 'B': 1, 'C': 1, 'D': 0}, 'power': 0},
	{'imageSprite': 'E', 'trees': {'A': 0, 'B': 0, 'C': 2, 'D': 0}, 'animals': {'A': 0, 'B': 0, 'C': 1, 'D': 1}, 'power': 0},
	{'imageSprite': 'E', 'trees': {'A': 0, 'B': 0, 'C': 0, 'D': 2}, 'animals': {'A': 1, 'B': 0, 'C': 0, 'D': 1}, 'power': 0}
]

var questCards := []

