extends Node2D

var cardPrefab = preload('res://prefabs/card.tscn')
var hold_card := false
var held_card

var deck := []
var hand := []
var cardPiles := [[], [], []]
var turn := 0
var numberOfCardsPlayed := 0
var score := 0
var maxZIndex := 0

func _ready():
	randomize()
	deck = Cards.basicCards.duplicate()
	init_turn()

func init_turn():
	numberOfCardsPlayed = 0
	rearange_hand()
	for index in range(5-hand.size()):
		Lock.lock = true
		await get_tree().create_timer(0.2).timeout
		Lock.lock = false
		draw()

func draw():
	if (deck.size() > 0):
		var cardIndex = randi_range(0, deck.size()-1)
		var drawed_card = deck.pop_at(cardIndex)
		hand.append(add_card(drawed_card, $Hand.get_child(hand.size()).global_position))
		$UI/Seeds.set_text(str(deck.size()))
	else :
		end_year()

func rearange_hand():
	for cardIndex in range(hand.size()) :
		hand[cardIndex].set_basic_pos($Hand.get_child(cardIndex).global_position)

func add_card(stats, cardPosition:Vector2 = Vector2(0,0)):
	var newCard = cardPrefab.instantiate()
	newCard.init(self, stats, cardPosition)
	$Cards.add_child(newCard)
	return newCard

func pickup_card(card):
	if (!hold_card) :
		card.set_z_index(maxZIndex)
		maxZIndex += 1
		held_card = card
		hold_card = true

func drop_card(_card):
	if (hold_card) :
		held_card = null
		hold_card = false

func _process(_delta):
	if(hold_card):
		held_card.position = get_global_mouse_position()
	if(Input.is_action_just_pressed("ui_accept") and $UI/GameOver.visible) :
		get_tree().change_scene_to_file("res://menu.tscn")

func _on_pile_input_event(_viewport, event, _shape_idx, pileIndex):
	if (event is InputEventMouseButton and event.pressed and event.button_index == MOUSE_BUTTON_LEFT) :
		if (hold_card and cardPiles[pileIndex].size() == turn) :
			cardPiles[pileIndex].append(held_card)
			hand.erase(held_card)
			held_card.position = $PlayZone.get_child(pileIndex).get_child(turn).global_position
			held_card.played()
			held_card = null
			hold_card = false
			numberOfCardsPlayed += 1
			if (numberOfCardsPlayed == 3) :
				end_turn()

func end_turn():
	turn += 1
	if (turn != 4):
		init_turn()
	else :
		end_year()

func end_year():
	for zone in range(3):
		Lock.lock = true
		await get_tree().create_timer(0.8).timeout
		Lock.lock = false
		count_score_and_bonus(zone)
		
	maxZIndex = 0
	for cardIndex in range(hand.size()) :
		hand[cardIndex].set_z_index(maxZIndex)

	if (deck.size() == 0) :
		$UI/GameOver.visible = true
		$Cards.visible = false
		Lock.lock = true
	else :
		deck.shuffle()
		clean_board()
		turn = 0
		init_turn()

func count_score_and_bonus(zone):
	var zoneScore := 0
	var seeds := 0
	var trees = {'A': 0, 'B': 0, 'C': 0, 'D': 0}
	var animals = {'A': 0, 'B': 0, 'C': 0, 'D': 0}
	
	for card in cardPiles[zone] :
		for tree in card.trees :
			trees[tree] += card.trees[tree]
		for animal in card.animals :
			animals[animal] += card.animals[animal]
			
	for tree in trees :
		zoneScore += trees[tree] * trees[tree]
		if trees[tree] >= 4 : 
			add_seed_4()
			seeds += 1

	var missingOneAnimal := false
	for animal in animals :
		if(animals[animal] == 0) :
			missingOneAnimal = true
	if (!missingOneAnimal) :
		for index in range(4) :
			add_seed()
		seeds += 4
	
	if (seeds > 0) :
		$PlayZone.get_child(zone).get_node('Seeds').visible = true
		$PlayZone.get_child(zone).get_node('Seeds').set_text('+' + str(seeds) + ' seeds')
	$PlayZone.get_child(zone).get_node('Score').visible = true
	$PlayZone.get_child(zone).get_node('Score').set_text('+' + str(zoneScore))
	$PlayZone.get_child(zone).popup_time = 1.5
	
	score += zoneScore
	$UI/Score.set_text(str(score))
	$UI/Seeds.set_text(str(deck.size()))

func add_seed_4():
	deck.append(Cards.fourTreeCards[randi_range(0, Cards.fourTreeCards.size()-1)].duplicate())

func add_seed():
	deck.append(Cards.basicCards[randi_range(0, Cards.basicCards.size()-1)].duplicate())

func clean_board():
	for zone in range(3) :
		for card in cardPiles[zone] :
			card.queue_free()
	cardPiles = [[], [], []]
